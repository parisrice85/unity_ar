# NieR:Automata AR
![image](/uploads/f430fc50703969e8ac5dd661f1bf1c7d/NieR_Automata_Title.png)
### 장르

    방치형 RPG AR

### 개발 일정	

    2023.03.24 ~ 2023.03.27
    
### 게임 플랫폼

    (Android 7.0 Nougat 이상)

### 사용 프로그램	

    Unity 2021.3.21f1, Visual Studio 2022 17.4.5, 
    Adobe Photoshop 2020 21.1.1, JetBrains Rider 2022.3.2

### 타깃 사용자

	새로운 형태의 Nier:Automata에 관심이 있는 사람

### 게임 스토리	

    먼 미래. 이성인의 침략에 인류는 지상에서 나와 달로 도망쳤다.
    인류 측은 지구 탈환을 위해 신형 안드로이드 ‘요르하’ 부대를 
    투입하였다. 부대의 안드로이드의 하나인 ‘2b’는 지구로 강하 중 
    갑작스러운 기류에 휩싸이게 되는데..

### 흥미요소

    1. 플랫폼 공간을 벗어나 AR Foundation 라이브러리를 활용한 현실기반, 공간, 오브젝트 AR
    인식을 통해서 해당 공간을 전장으로 구현
    2. 게임의 재미와 몰입도를 높이기 위하여 툰쉐이더 카툰 랜더링 그래픽으로 
    캐릭터 구현

    3. 방치형 전투와 직관적이고 단순한 조작을 통해 게임의 중심 재미 요소를 증대

### 게임의 컨셉(기획의도)

    1. 인기 IP 오토마타의 캐릭터 2b를 현실 기반 AR 3D 캐릭터로 구현하여 
    (대화, 선물, 캐릭터 터치 등 추후 개발예정) 상호 작용이 가능한 게임 시스템을 통해 
    게임의 재미 요소 증대

    2. 방치형을 선호하는 유저와 육성을 선호하는 유저, 캐릭터와 상호작용을 중시하는 
    유저에게 적합한 콘텐츠로 구성



### 핵심 기술
-   AR PLANE 인식 후 감지된 바닥 표면에 몬스터 자동 생성 구현
-	몬스터 자동 생성의 경우 다양한 모바일 플랫폼 환경과 그에 따른 최적화를 위해 오브젝트 풀링 기법 활용하여 구현
-	AI 기능 기반의 2b VS 몬스터 자동 전투 구현
-	게임 플레이 시 2b 캐릭터 이동을 AR Raycast를 활용하여 Drag touch 방식으로 구현
-	게임 몰입감 증대를 위하여 카툰 쉐이더 그래픽 처리 및 2b의 헤어와 의상에 물리효과 적용

### 문제 발생
-	카메라의 AR PLANE 인식 문제
-	캐릭터가 휴대폰 화면 밖으로 이동 시 유저가 직접 찾아야 하는 문제
-	물리효과로 인한 프레임 저하

 ### 게임 조작방법
    휴대폰 터치 방식
![image](https://gitlab.com/parisrice85/unity_ar/uploads/86086037c293a64ca516917d906361ef/KakaoTalk_20230329_124946676.jpg)
